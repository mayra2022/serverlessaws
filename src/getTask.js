const AWS = require('aws-sdk');

const getTask = async (event) => {

    try {
        const dynamodb = new AWS.DynamoDB.DocumentClient();

        const result = await dynamodb.scan({
            TableName: 'taskTable'
        }).promise()

        const tasks = result.Items;

        console.log(tasks);

        return{
            status: 200,
            body: {
                tasks
            }
        }
    } catch (error) {
        console.log(error);
    }
}



module.exports = {getTask};